import configparser
import subprocess
import sys, getopt
import json
import os

AWS_ACCOUNTS = {'dev': '748630521564', 'prod': '394233314359'}
DEFAULT_CONFIG_DIR = os.path.join(os.getenv('HOME'), '.aidoc/')
DEFAULT_CONFIG = os.path.join(DEFAULT_CONFIG_DIR, 'mfa-default.ini')
profile = ''
account_alias = None
account_id = None
username = None


def usage():
    print('test.py -p <profile> -a <[dev | prod]> -u <aws username> -t <token> ')
    sys.exit(1)


def read_defaults():
    global account_id, profile, username
    if os.path.exists(DEFAULT_CONFIG):
        config = configparser.ConfigParser()
        config.read(DEFAULT_CONFIG)

        default = config['default']
        account_id = default.get('account_id')
        profile = default.get('profile')
        username = default.get('username')


def write_defaults(account_id, profile, username):
    config = configparser.ConfigParser()
    config.add_section('default')
    config['default']['account_id'] = account_id
    config['default']['profile'] = profile
    config['default']['username'] = username
    if not os.path.exists(DEFAULT_CONFIG_DIR):
        os.mkdir(DEFAULT_CONFIG_DIR)

    with open(DEFAULT_CONFIG, 'w') as configfile:
        config.write(configfile)


def main(argv):
    global profile, account_id, account_alias, username
    token = ''
    read_defaults()

    try:
        opts, args = getopt.getopt(argv, "hp:t:a:u:", ["account_id=", "username=" "profile=", "token="])
    except getopt.GetoptError:
        usage()
    for opt, arg in opts:
        if opt == '-h':
            usage()
        elif opt in ("-p", "--profile"):
            profile = arg
        elif opt in ("-t", "--token"):
            token = arg
        elif opt in ("-a", "--account"):
            account_alias = arg
            if account_alias not in ["dev", "prod"]:
                usage()
            account_id = AWS_ACCOUNTS[account_alias]
        elif opt in ("-u", "--username"):
            username = arg
    if '' in [profile, token]:
        usage()

    if '' in [account_id, username]:
        usage()

    aws_config_file = os.path.join(os.getenv('HOME'), '.aws/config')
    if not os.path.exists(aws_config_file):
        print("AWS config file is missing: {}".format(aws_config_file))
        exit(1)

    config = configparser.ConfigParser()
    config.read(aws_config_file)
    try:
        config["profile " + profile]
    except Exception:
        print("profile {} is missing from {}".format(profile, aws_config_file))
        exit(1)

    print("Getting 2fa for: account: {}; username: {}; profile: {}; token: {}".format(
        account_id, username, profile, token))
    cmd = "aws sts get-session-token --serial-number arn:aws:iam::{}:mfa/{} --profile {} --token-code {}".format(
        account_id, username, profile, token)
    MyOut = subprocess.Popen(cmd.split(),
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
    stdout, stderr = MyOut.communicate()
    if MyOut.returncode != 0:
        print("Fail to MFA: " + str(stdout))
        exit(1)

    output_json = json.loads(stdout)

    config["profile mfa-"+profile] = ({'aws_access_key_id': output_json['Credentials']['AccessKeyId'],
                            'aws_secret_access_key': output_json['Credentials']['SecretAccessKey'],
                            'aws_session_token': output_json['Credentials']['SessionToken'],
                            'region': config["profile " + profile]['region']})

    with open(aws_config_file, 'w') as configfile:
        config.write(configfile)

    write_defaults(account_id, profile, username)
    print("Updated access key for account: {} on profile {}".format(account_id, profile))


if __name__ == "__main__":
    main(sys.argv[1:])
