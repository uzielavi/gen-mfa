# gen-mfa 

Utility to generate Multi Factor Authentication keys and update environment for using it.
The script will perform 2FA and write the results to ~/.aws/config file.
It will copy the profile and create an profile named "profile mfa-<profile>" setup the following keys:

    [profile mfa-<profile name>]
    aws_access_key_id = XXXX
    aws_secret_access_key = YYYY
    aws_session_token = ZZZZ
    region = us-east-2
    

## Pre-requisite:
have ~/.aws/config file with the following entry
    
    [profile <profile name>]
    aws_access_key_id = XXXX
    aws_secret_access_key = YYYY
    region = us-east-2
    
## Usage:

    gen-mfa.py -p <profile> -a <account alias name: [dev | prod]> -u <aws username> -t <token> 

## Install the utility using:
    pip install aidoc.gen-mfa

## Run it using:
    python3 -m aidoc.gen-mfa <params>
    
The script will cache your parameters under ```~/.aidoc/mfa-default.ini``` 
so further calls can omit all parameters except the -t
