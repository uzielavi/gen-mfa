import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="aidoc.genmfa",
    version="0.0.2",
    author="Avi Uziel",
    author_email="avi.uziel@aidoc.com",
    description="Utility for updating AWS MFA credentials",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/uzielavi/gen-mfa/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
    
    ]
)